import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")

    property int defMargin: 10

    SwipeView {
        id: view
        currentIndex: indicator.currentIndex
        anchors.fill: parent
        orientation: Qt.Vertical

        My_Page {
            id: page_red
            backgroundColor: "red"
            Text {
                id: name
                text: qsTr("RED")
                color: "darkred"
                anchors.centerIn: parent
                font.pointSize: 20
            }
        }

        My_Page {
            id: page_green
            backgroundColor: "green"
            Text {
                id: name_1
                text: qsTr("GREEN")
                color: "darkgreen"
                anchors.centerIn: parent
                font.pointSize: 20
            }
        }
        My_Page {
            id: page_yellow
            backgroundColor: "yellow"
            Text {
                id: name_2
                text: qsTr("YELLOW")
                color: "orange"
                anchors.centerIn: parent
                font.pointSize: 20
            }
        }

    }
    PageIndicator {
        id: indicator
        interactive: true
        transform: Rotation { origin.x: 20; angle: 90}
        count: view.count
        currentIndex: view.currentIndex

        anchors.left: view.left
        anchors.verticalCenter: parent.verticalCenter
    }
}
