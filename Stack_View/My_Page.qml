import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Page {
    id: root
    property alias backgroundColor: back_fon.color
    property alias buttonText1: buttonText1.text
    property alias buttonText2: buttonText2.text
    signal button1Clicked()
    signal button2Clicked()
    signal backButtonClicked()

    background: Rectangle {
        id: back_fon
    }

    Button {
        id: buttonText1
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: {
            button1Clicked()
        }
    }
    Button {
        id: buttonText2
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        onClicked: {
            button2Clicked()
        }
    }


    Button {
        id: back_button
        visible: stack_view.currentItem != page_red
        text: "Back"
        onClicked: {
            backButtonClicked()
            back_button.visible = true
        }
    }
}
