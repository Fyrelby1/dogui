import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")

    property int defMargin: 10

    StackView {
        id: stack_view
        anchors.fill: parent
        initialItem: page_red
    }

    My_Page {
        id: page_red
        backgroundColor: "red"
        buttonText1: "To Yellow"
        buttonText2: "To Green"
        onButton1Clicked: {
            stack_view.push(page_yellow)
        }
        onButton2Clicked: {
            stack_view.push(page_green)
        }
        onBackButtonClicked: {

        }
    }

    My_Page {
        id: page_green
        visible: false
        backgroundColor: "green"
        buttonText1: "To Red"
        buttonText2: "To Yellow"
        onButton1Clicked: {
            stack_view.pop(page_red)
        }
        onButton2Clicked: {
            stack_view.push(page_yellow)
        }
        onBackButtonClicked: {
            stack_view.pop()
        }
    }

    My_Page {
        id: page_yellow
        visible: false
        backgroundColor: "yellow"
        buttonText1: "To Green"
        buttonText2: "To Red"
        onButton1Clicked: {
            stack_view.push(page_green)
        }
        onButton2Clicked: {
            stack_view.pop(page_red)
        }

        onBackButtonClicked: {
            stack_view.pop()
        }
    }
}
