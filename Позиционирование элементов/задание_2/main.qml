import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3

Window {
    id: win
    width: 360
    height: 640
    visible: true
    title: qsTr("Loyouts")
    Rectangle {
        id: head
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 70
        width: 360
        color: "#ffdf84"
        Text{
            id: headtext
            text:"Header"
            anchors.centerIn: head
            font.pixelSize: 14
        }
    }
    Rectangle
    {
        id:footer
        color: "#c2c2c2"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 70
        width: 360
    }
    Rectangle {
        id: content
        width: 350
        height: 400
        color: "#54ff9f"
        anchors.top: head.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: footer.top
        anchors.margins: 10
        Text{
            id: contenttext
            text:"Content"
            anchors.centerIn: content
            font.pixelSize: 14
        }
    }
    Rectangle {
        id: butn1
        width: 110
        height: 70
        color: "#54ff9f"
        anchors.bottom: footer.bottom
        anchors.left: footer.left
        Text{
            id: butn1text1
            text:"1"
            anchors.centerIn: butn1
            font.pixelSize: 14
        }

    }
    Rectangle {
        id: butn2
        width: 110
        height: 70
        color: "#77dd77"
        anchors.bottom: footer.bottom
        anchors.centerIn: footer
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        Text{
            id: butn1text2
            text:"2"
            anchors.centerIn: butn2
            font.pixelSize: 14
        }

    }
    Rectangle {
        id: butn3
        width: 110
        height: 70
        color: "#50c878"
        anchors.bottom: footer.bottom
        anchors.right: footer.right
        Text{
            id: butn1text3
            text:"3"
            anchors.centerIn: butn3
            font.pixelSize: 14
        }

    }
}
