import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 600
    height: 600
    visible: true
    title: qsTr("Task 1")
    Rect{
        id: body
        comColor: "#7ba05b"
        comWidth: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Rect{
        id: kluv
        comColor: "yellow"
        comHeight: 50
        anchors.right: head.left
        anchors.top: head.top
        anchors.rightMargin: 100
        anchors.topMargin: 60

    }
    Rect{
        id: head
        comColor: "#7ba05b"
        comHeight: 200
        anchors.right: body.left
        anchors.bottom: body.top
        anchors.rightMargin: 100
        anchors.bottomMargin: 100
    }

    Rect{
        id: eye
        comColor: "#4f273a"
        comHeight:50
        comWidth: 50
        anchors.top: head.top
        anchors.right: head.right
        anchors.rightMargin: -20
        anchors.topMargin: 20
    }
    Rect{
        id: leg_1
        comColor: "yellow"
        comHeight: 75
        comWidth: 25
        anchors.top: body.bottom
        anchors.right: body.left
        anchors.topMargin: 100
    }
    Rect{
        id: leg_2
        comColor: "yellow"
        comHeight: 75
        comWidth: 25
        anchors.top: body.bottom
        anchors.right: body.left
        anchors.topMargin: 100
        anchors.rightMargin: -100
    }
    Rect{
        id: leg_3
        comColor: "yellow"
        comHeight: 25
        comWidth: 55
        anchors.right: leg_1.left
        anchors.bottom: leg_1.bottom
        anchors.rightMargin: 50
        anchors.bottomMargin: -50
    }
    Rect{
        id: leg_4
        comColor: "yellow"
        comHeight: 25
        comWidth: 55
        anchors.right: leg_2.left
        anchors.bottom: leg_2.bottom
        anchors.rightMargin: 50
        anchors.bottomMargin: -50
    }
}
