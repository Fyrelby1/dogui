import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3

Window {
    id:win
    minimumWidth: 375
    width: 375
    height: 720
    visible: true
    title: qsTr("Layouts")
    GridLayout{
        id: loColumn
        columns: 1
        rows: 3
        anchors.fill:parent
        Rect{
            color: "#ccccff"
            id: comp_1
            anchors.top: loColumn.top
            Layout.fillWidth: true
            Text {
                id: text_com1
                text: "How to draw an owl?"
                anchors.centerIn: comp_1
            }
        }
        Rect{
            color: "#fba0e3"
            id: comp_2
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 10
            Text {
                id: name
                text: qsTr("img")
                anchors.centerIn: comp_2
            }
            Image {
                id: photoPreview
                anchors.centerIn: comp_2
                source: "img"
            }
        }
        Rect{
            id: comp_3
            color: "white"
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            RowLayout{
                spacing: 6
                anchors.fill:parent
                Rect{
                    id: comp_4
                    Layout.alignment: Qt.AlignLeft
                    Layout.fillWidth: true
                    color: mouse.containsMouse ? "#f754e1": "#ba55d3"
                    Text {
                        text: "1"
                        anchors.centerIn: comp_4
                    }
                    MouseArea {
                        id:mouse
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked:{
                            console.log("area clicked com4")
                            photoPreview.source = "qrc:/1.jpg"
                            text_com1.text = "Stage 1"
                            comp_4.opacity = 1
                            comp_5.opacity = 0.5
                            comp_6.opacity = 0.5
                        }
                    }
                }
                Rect{
                    id: comp_5
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    color: mouse1.containsMouse ? "#f754e1": "#ba55d3"
                    Text {
                        text: "2"
                        anchors.centerIn: comp_5
                    }
                    MouseArea {
                        id:mouse1
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked:{
                            console.log("area clicked com5")
                            photoPreview.source = "qrc:/2.jpg"
                            text_com1.text = "Stage 2"
                            comp_4.opacity = 0.5
                            comp_5.opacity = 1
                            comp_6.opacity = 0.5
                        }
                    }
                }
                Rect{
                    id: comp_6
                    Layout.alignment: Qt.AlignRight
                    Layout.fillWidth: true
                    color: mouse2.containsMouse ? "#f754e1": "#ba55d3"
                    Text {
                        text: "3"
                        anchors.centerIn: comp_6
                    }
                    MouseArea {
                        id:mouse2
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked:{
                            console.log("area clicked com6")
                            photoPreview.source = "qrc:/3.jpg"
                            text_com1.text = "Stage 3"
                            comp_4.opacity = 0.5
                            comp_5.opacity = 0.5
                            comp_6.opacity = 1
                        }
                    }
                }
            }
        }
    }

}


