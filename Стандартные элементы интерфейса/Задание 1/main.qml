import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

ApplicationWindow {
    id: win
    visible: true
    minimumWidth: 375
    width: 375
    height: 720
    title: "Login Page"
    background: Rectangle {
        gradient: Gradient {
            GradientStop { position: 0; color: "#ffffff" }
            GradientStop { position: 1; color: "#fdeaa8" }
        }
    }

    Rectangle {
        id: loginForm
        width: 280
        height: 250
        anchors.centerIn: parent
        color: "transparent"

        Column {
            spacing: 5
            anchors.fill: parent

            TextField {
                id: usernameField
                placeholderText: "Username"
                font.pixelSize: 16
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
            }

            TextField {
                id: passwordField
                placeholderText: "Password"
                font.pixelSize: 16
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                echoMode: TextInput.Password
            }

            RowLayout {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - 40
                spacing: 10

                Button {
                    id: loginButton
                    text: "Log In"
                    Layout.alignment: Qt.AlignCenter
                    onClicked: {
                        console.log("Login clicked with username:", usernameField.text)
                    }

                }

                Button {
                    id: clearButton
                    text: "Clear"
                    Layout.alignment: Qt.AlignCenter
                    onClicked: {
                        usernameField.text = ""
                        passwordField.text = ""
                    }
                }
            }
        }
    }
}
