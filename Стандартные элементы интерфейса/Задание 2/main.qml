import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

ApplicationWindow {
    id: win
    visible: true
    minimumWidth: 375
    width: 375
    height: 720
    title: "Login Page"

    Column {
        anchors.fill: parent
        anchors.margins: 20
        spacing: 20

        Text {
            text: "Enter your password:"
            font.pixelSize: 16
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Rectangle {
            id: passwordField
            width: parent.width - 40
            height: 60
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#FFFFFF"
            property string text: ""

            RowLayout {
                anchors.fill: parent
                anchors.margins: 10
                spacing: 10

                Repeater {
                    model: 6
                    Label {
                        width: 20
                        height: 40
                        text: "*"
                        font.pixelSize: 36
                        color: index <passwordField.text.length ? "#000000" : "#D3D3D3"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
        }
        GridLayout {
            id: keypad
            rows: 4
            columns: 3
            width: parent.width
            Repeater {
                model: 12
                Button {
                    text: index === 9? "" : index === 10? "0" : index === 11? "Clear" : (index + 1).toString()
                    onClicked: {
                        if (text === "Clear") {
                            passwordField.text = "";
                        } else {
                            if (passwordField.text.length < 6) {
                                passwordField.text += text;
                            }
                        }
                    }
                }
            }



        }
    }
}
